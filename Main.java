package Company;

public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager("Jan", "Kowalski");
        manager.addEmployee(new Expert("Adam", "Nowak"));
        manager.addEmployee(new Programmer("Zenon", "Mickiewicz"));

        manager.printSalary();
    }
}
