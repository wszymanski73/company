package Company;

import java.util.LinkedList;
import java.util.List;

public class Manager extends Employee {
    private List<Employee> employees = new LinkedList<>();

    public Manager(String firstName, String lastName) {
        super(firstName, lastName);
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    @Override
    public void printSalary() {
        System.out.println(this + ", salary: " + 10000.3);
        employees.forEach(Employee::printSalary);
    }
}
