package Company;

import java.util.ArrayList;
import java.util.List;

public class HeadDepartment implements Department {
    private List<Department> childDepartments;

    public HeadDepartment() {
        this.childDepartments = new ArrayList<>();
    }

    @Override
    public void printDepartmentName() {
        System.out.println("Head Department");
        childDepartments.forEach(Department::printDepartmentName);
    }

    public void addDepartment(Department department) {
        childDepartments.add(department);
    }

    public void removeDepartment(Department department) {
        childDepartments.remove(department);
    }
}
