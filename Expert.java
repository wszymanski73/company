package Company;

public class Expert extends Employee {
    public Expert(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void printSalary() {
        System.out.println(this + ", salary: " + 5678.9);
    }
}
