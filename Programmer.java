package Company;

public class Programmer extends Employee {
    public Programmer(String firstName, String lastName) {
        super(firstName, lastName);
    }

    @Override
    public void printSalary() {
        System.out.println(this + ", salary: " + 4000.2);
    }
}
